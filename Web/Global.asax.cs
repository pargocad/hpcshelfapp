﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;

using System.Data.Entity;
using Web.DAO;
using Web.DAO.Builder;
using Web.Filters;

namespace Web {
    public class Global : HttpApplication {

        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AnnouncedFilterAttribute());
            //filters.Add(new AuthFilterAttribute());
        }

        protected void Application_Start() {
            //Database.SetInitializer (new DropCreateDatabaseAlways <JobsContext>());
            //Database.SetInitializer (new DropCreateDatabaseIfModelChanges <JobsContext>());
            Database.SetInitializer(new MySqlSeeder());
            RegisterGlobalFilters(GlobalFilters.Filters);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
