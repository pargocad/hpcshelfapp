﻿using System;
using System.Web.Mvc;
using Web.Controllers;

namespace Web.Filters {
    public class AuthFilterAttribute: ActionFilterAttribute {

        public AuthFilterAttribute() { }

        public override void OnActionExecuting(ActionExecutingContext filterContext) {
            base.OnActionExecuting(filterContext);
            object user = filterContext.HttpContext.Session[SessionController.keys[0]];
            if (user == null) {
                filterContext.Result = new RedirectToRouteResult (
                    new System.Web.Routing.RouteValueDictionary (
                        new { controller = "Session", action = "Login" }
                    )
                );
            }
        }
    }
}
