﻿using System;
using System.Web.Mvc;

namespace Web.Filters {
    public class AnnouncedFilterAttribute: ActionFilterAttribute {

        public override void OnActionExecuting(ActionExecutingContext filterContext) {
            base.OnActionExecuting(filterContext);

            filterContext.Controller.ViewBag.Announced = Announced();
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext) {
            base.OnActionExecuted(filterContext);

            this.UpdateAnnounced();
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext) {
            base.OnResultExecuting(filterContext);
        }
        public override void OnResultExecuted(ResultExecutedContext filterContext) {
            base.OnResultExecuted(filterContext);
        }

        private string Announced() { return "<h2>Warning!!!</h2>"; }
        private void UpdateAnnounced() { /* TODO */ }
    }
}
