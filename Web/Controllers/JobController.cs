﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.DAO;
using System.Data.Entity;
using PagedList;
using Web.Filters;

namespace Web.Controllers {
    [AuthFilter]
    public class JobController : Controller {
        private readonly UserDAO dao_users;
        private readonly JobDAO dao_jobs;
        private readonly int numRows = 7;

        public JobController() { this.dao_jobs = new JobDAO(); this.dao_users = new UserDAO(); }

        public ActionResult Index(int? page) {
            List<Job> jobs = (List<Job>)this.dao_jobs.List(); 
            List<Job> sortedJobs = jobs.OrderBy(p => p.Birth).ToList();

            int iPage = page ?? 1;
            return View("Index", sortedJobs.ToPagedList(iPage, numRows));
        }

        public ActionResult Show(int? id) { 
            Job job = this.GetJob(id);
            if (job == null) { return RedirectToAction("Index"); }
            return View(job);
        }

        public ActionResult New() { 
            Job job = new Job { UserId = (int) Session[SessionController.keys[1]] };
            ViewBag.UsersNames = UsersOrderByUserName;
            return View(job); 
        }

        [HttpPost, ActionName("New")]
        public ActionResult Create([Bind(Include = "JobName, UserId")]Job job) {
            job.Birth = DateTime.Now;
            if (ModelState.IsValid && dao_jobs.Add(job)) return RedirectToAction("Index");

            ViewBag.UsersNames = UsersOrderByUserName;
            return View(job);
        }

        public ActionResult Edit(int? id) {
            Job job = this.GetJob(id);
            if (job == null || !SessionController.Mine(job.UserId, this)) { return RedirectToAction("Index"); } 
            return View(job); 
        }

        [HttpPost, ActionName("Edit")]
        public ActionResult Update([Bind(Include = "Id,UserId,JobName,Birth")]Job job) {
            if (job != null || SessionController.Mine(job.UserId, this))
                if (ModelState.IsValid && dao_jobs.Update(job))
                    return RedirectToAction("Index");
            return View(job);
        }

        public ActionResult Delete(int? id) { return RedirectToAction("Index"); }

        [HttpDelete]
        public ActionResult Delete(int id, FormCollection collection) {
            Job job = this.dao_jobs.FindById(id);
            if (SessionController.Mine(job.UserId, this) && this.dao_jobs.Delete(id))
                return RedirectToAction("Index", new { page = collection["hiddenPage"] });

            return RedirectToAction("Index", "Home");
        }

        private Job GetJob(int? id) { if (id == null) return null; return this.dao_jobs.FindById((int)id); }
        private SelectList UsersOrderByUserName { get { return new SelectList(this.dao_jobs.Users(true), "Id", "UserName"); } }
    }
}

/* 
 * SelfList(SessionConfig.GetSessionUserID(this));
 * User user = this.dao_users.FindByUserName((string) Session[SessionConfig.keys[0]]);
 * try { job.Birth = DateTime.Parse(Request.Form["Birth"]); } catch { ModelState.AddModelError("Birth", "Date format error!!!"); } 
 */
