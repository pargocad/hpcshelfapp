﻿using System;
using System.Web.Mvc;
using Web.Models;
using Web.DAO;
using Web.Filters;

namespace Web.Controllers {
    public class UserController : Controller {
        private UserDAO dao;

        public UserController() { this.dao = new UserDAO(); }

        public ActionResult Index() { return RedirectToAction("Account/" + (Session[SessionController.keys[0]])); }

        public ActionResult New() { return View(); }

        [HttpPost, ActionName("New")]
        public ActionResult Create([Bind(Include = "UserName,Password")]User user) {
            if (ModelState.IsValid && dao.Add(user)) {
                SessionController.SetSession(user, this);
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("UserName", "Please, choose other username");
            return View();
        }

        [AuthFilter]
        public ActionResult Account(string username) {
            User user = this.dao.FindByUserName(username);
            if (user == null || !SessionController.Mine(user.Id, this)) { return RedirectToAction("Index"); }
            return View(user);
        }

        [AuthFilter, HttpPost, ActionName("Account")]
        public ActionResult Update([Bind(Include = "Id,UserName,Password")]User user) {
            if (user == null) return RedirectToAction("Index");
            if (ModelState.IsValid && SessionController.Mine(user.Id, this)) {
                SessionController.SetSession(user, this);
                dao.Update(user);
                ViewBag.Updated = true;
            }
            return View(user);
        }

        [AuthFilter]
        public ActionResult Delete(int? id) { return RedirectToAction("Index"); }

        [AuthFilter, HttpDelete]
        public ActionResult Delete(int id, FormCollection collection) {
            if (SessionController.Mine(id, this)) {
                this.dao.Delete(id);
                SessionController.SetSession(null, this);
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Account");
        }
    }
}