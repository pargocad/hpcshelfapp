﻿using System;
using System.Web.Mvc;
using Web.Models;
using Web.DAO;
using Web.Filters;

namespace Web.Controllers {
    public class SessionController : Controller {
        private UserDAO dao;

        public SessionController() { this.dao = new UserDAO(); }

        public ActionResult Index() { return RedirectToAction("Login"); }

        public ActionResult Login() { return View(); }

        [HttpPost, ActionName("Login")]
        public ActionResult SignIn([Bind(Include = "UserName,Password")]User user) {
            User login = dao.Find(user.UserName, user.Password);
            if (login != null) {
                SessionController.SetSession(login, this);
                return RedirectToAction("Index", "Home");
            }
            return View(user);
        }

        [AuthFilter]
        public ActionResult Logout() {
            SessionController.SetSession(null, this);
            return RedirectToAction("Login");
        }

        private User GetUser(int? id) {
            if (id == null) return null;
            return this.dao.FindById((int)id);
        }

        /* statics codes */
        public static readonly string[] keys = { "user_username", "user_id" };

        public static bool Mine(int? id, Controller controller) {
            if (id == null || controller.Session[SessionController.keys[1]] == null) return false;
            return (int)controller.Session[SessionController.keys[1]] == id;
        }
        public static void SetSession(User user, Controller controller) {
            if (user != null) {
                controller.Session[SessionController.keys[0]] = user.UserName;
                controller.Session[SessionController.keys[1]] = user.Id;
                return;
            }
            controller.Session.Remove(SessionController.keys[0]);
            controller.Session.Remove(SessionController.keys[1]);
        }
        public static int? GetSessionUserID(Controller controller) {
            return (int?)controller.Session[SessionController.keys[1]];
        }
    }
}