﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Web.Models;

namespace Web.DAO {
    public class UserDAO {
        public bool Add(User user) {
            using (var repo = new RepoContext()) {
                if(repo.Users.Where(u => u.UserName == user.UserName).FirstOrDefault() == null) {
                    user.Password = UserDAO.GetMd5(user.UserName, user.Password);
                    repo.Users.Add(user);
                    repo.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public IList<User> List() {
            using (var repo = new RepoContext()) {
                return repo.Users.ToList();
            }
        }

        public User Find(string username, string password) {
            using (var repo = new RepoContext()) {
                string hash = UserDAO.GetMd5(username, password);
                return repo.Users.FirstOrDefault(u => u.UserName == username && u.Password == hash);
            }
        }

        public User FindById(int id) {
            using (var repo = new RepoContext()) {
                return repo.Users
                    .Where(user => user.Id == id)
                    .FirstOrDefault();
            }
        }

        public User FindByUserName(string username) {
            using (var repo = new RepoContext()) {
                return repo.Users
                    .Where(user => user.UserName == username)
                    .FirstOrDefault();
            }
        }

        public void Update(User user) {
            using (var repo = new RepoContext()) {
                user.Password = UserDAO.GetMd5(user.UserName, user.Password);
                repo.Entry(user).State = System.Data.Entity.EntityState.Modified;
                repo.SaveChanges();
            }
        }

        public void Delete(int id) {
            using (var repo = new RepoContext()) {
                User user = repo.Users.Find(id);
                repo.Users.Remove(user);
                repo.SaveChanges();
            }
        }
        public static string GetMd5(string username, string password) {
            MD5 md5 = MD5.Create();
            byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(username+password));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++) {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        /* public static bool Verify(string username, string password, string md5) {
            string md5Test = UserDAO.GetMd5(username, password);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(md5Test, md5)) return true;
            return false;
        }*/
    }
}
