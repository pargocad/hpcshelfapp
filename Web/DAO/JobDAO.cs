﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using Web.Models;

namespace Web.DAO {
    public class JobDAO {
        public bool Add(Job job) {
            using (var repo = new RepoContext()) {
                repo.Jobs.Add(job);
                return repo.SaveChanges()!=0;
            }
        }

        public IList<Job> List() {
            using (var repo = new RepoContext()) {
                //return repo.Jobs.ToList(); //.Include("User").ToList();
                return repo.Jobs.Include("User").ToList();
            }
        }

        public IList<Job> SelfList(int? id) {
            using (var repo = new RepoContext()) {
                return repo.Jobs.Where(job => job.UserId == id).ToList();
            }
        }

        public IList<User> Users(bool sorted) {
            using (var repo = new RepoContext()) {
                List<User> users = (List<User>) repo.Users.ToList();
                if (!sorted) return users;
                return users.OrderBy(u => u.UserName).ToList();
            }
        }

        public Job FindById(int id) {
            using (var repo = new RepoContext()) {
                return repo.Jobs//.Include("User")
                    .Where(job => job.Id == id)
                    .FirstOrDefault();
            }
        }

        public bool Update(Job job) {
            using (var repo = new RepoContext()) {
                repo.Entry(job).State = System.Data.Entity.EntityState.Modified;
                return repo.SaveChanges()!=0;
            }
        }
        public bool Delete(int id) {
            using (var repo = new RepoContext()) {
                Job job = repo.Jobs.Find(id);
                repo.Jobs.Remove(job);
                return repo.SaveChanges()!=0;
            }
        }
    }
}