﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Web.Models;

namespace Web.DAO.Builder { // DropCreateDatabaseAlways | DropCreateDatabaseIfModelChanges 

    public class MySqlSeeder : DropCreateDatabaseIfModelChanges<RepoContext> {

        public static DateTime GetRandomDate(DateTime from, DateTime to) {
            Random rnd = new Random();
            var range = to - from;

            // NextDouble gives a number between 0.0 and 1.0
            var randTimeSpan = new TimeSpan((long)(rnd.NextDouble() * range.Ticks));

            return from + randTimeSpan;

        }

        // To use IDE code genrator to create overides use alt+insert
        protected override void Seed(RepoContext context) {
            string[] jobNames = { "Triangle Enumeration", "SSSP", "PageRank", "Maximal Cliques" };
            string[] userNames = { "Leslie Lamport", "Dijkstra", "Larry Page", "Leonhard Euler" };
            DateTime start = new DateTime(2004, 1, 1);

            DateTime end = new DateTime(2019, 11, 7);

            context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX idx_users_username  ON users (username)");

            List<User> users = new List<User>();

            for (int i = 0; i < userNames.Length; i++) {
                User user = new User {
                    UserName = userNames[i],
                    Password = UserDAO.GetMd5(userNames[i], "159753")
                };
                context.Users.Add(user);
                users.Add(user);
                context.SaveChanges();
            }

            for (int i = 0; i < userNames.Length; i++) {
                User user = users[i];
                Job job = new Job() {
                    JobName = jobNames[i],
                    Birth = GetRandomDate(start, end),
                    User = user
                };
                context.Jobs.Add(job);
                context.SaveChanges();
            }

            base.Seed(context);
        }
    }
}

