﻿using System;
using System.Data.Common;
using System.Data.Entity;
using Web.Models;
//using MySql.Data.Entity;

namespace Web.DAO {
    //[DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class RepoContext : DbContext {
        public DbSet<Job> Jobs { get; set; }
        public DbSet<User> Users { get; set; }

        public RepoContext() : base("connStr") { }
        public RepoContext(DbConnection existingConnection, bool contextOwnsConnection)
            :base(existingConnection, contextOwnsConnection) { }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder) {
        //    base.OnModelCreating(modelBuilder);
        //    //modelBuilder.Entity<Job>().MapToStoredProcedures();
        //}

    }
}

