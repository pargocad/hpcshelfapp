﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Models {
    public class JobConfig {
        [Display(Name = "Job Name")]
        public string JobName { get; set; }

        [Display(Name = "Birthday")]
        public DateTime? Birth { get; set; }

        [Display(Name = "User")]
        public int UserId { get; set; }
    }

    [MetadataType(typeof(JobConfig))]
    [Table("jobs")]
    public class Job {
        public Job() { this.Birth = DateTime.Now; }

        [Column("id")]
        public int Id { get; set; }

        [Column("job_name")]
        [Required(ErrorMessage = "Job name is required")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "JobName must have min length of {2} and max Length of {1}")]
        public string JobName { get; set; }

        [Column("birth")]
        public DateTime? Birth { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public override string ToString() {
            return "["+Id+":"+JobName+":"+UserId+"]";
        }
    }
}

