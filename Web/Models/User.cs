﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Models {
    public class UserConfig {
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }
    }

    [MetadataType(typeof(UserConfig))]
    [Table("users")]
    public class User {

        public User() {}

        [Column("id")]
        public int Id { get; set; }

        [Column("username")]
        [Required(ErrorMessage = "Username is required")]
        [StringLength(32, MinimumLength = 3, ErrorMessage = "Username must have min length of {2} and max Length of {1}")]
        public string UserName { get; set; }

        [Column("password")]
        [Required(ErrorMessage = "Password is required")]
        [StringLength(64, MinimumLength = 3, ErrorMessage = "Password must have min length of {2} and max Length of {1}")]
        public string Password { get; set; }

        public override string ToString() { return this.UserName; }
    }
}

