#!/bin/bash
app="./www"

echo "******** command line ***********"
echo "rm -rf $app"
  rm -rf $app
echo "rm -rf $app.tar.gz"
  rm -rf $app.tar.gz
echo "mkdir -p $app"
  mkdir -p $app

cp -a ./Web/bin $app
cp -a ./Web/Global.asax $app
cp -a ./Web/Views $app
cp -a ./Web/Web.config $app
cp -a ./Web/Content $app
cp -a ./Web/Scripts $app

echo "tar -czf $app.tar.gz $app"
tar -czf $app.tar.gz $app


echo "*********************************"
echo "For run:"
echo "   cd $app"
echo "   xsp4 --nonstop --port=8080"
echo "*********************************"
echo "For deploy: "
echo "   copy $app.tar.gz to target server and unpack"



